ARG PHP_VERSION=7.2

FROM shynome/nginx:1.5.2 as nginx_diy

FROM nginx:stable as nginx

FROM devilbox/php-fpm:${PHP_VERSION}-prod

# add nginx user for nginx
RUN adduser --system --no-create-home --shell /bin/false --group --disabled-login nginx
# nginx lib
COPY --from=nginx /usr/lib/nginx/ /usr/lib/nginx/
COPY --from=nginx /usr/sbin/ /usr/sbin/
COPY --from=nginx /etc/nginx/ /etc/nginx/

# shynome/nginx config
RUN rm -rf /etc/nginx/conf.d
COPY --from=nginx_diy /etc/nginx/conf.d /etc/nginx/conf.d
COPY --from=nginx_diy /etc/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=nginx_diy /scripts/nginx-init.sh /scripts/nginx-init.sh

# end nginx copy

WORKDIR /app

ENTRYPOINT [ "/bin/bash" ]

# config
ENV NGINX_MODE='thinkphp' \ 
    ROOT='/app' \
    FASTCGI='unix:/var/run/php-fpm.sock'
RUN mkdir -p \
    /var/log/php-fpm/ \
    /var/log/nginx/ /var/cache/nginx/

COPY rootfs /

RUN chmod +x /scripts/*.sh

CMD [ "/scripts/start.sh" ]
