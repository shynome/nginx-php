# 简介

为什么又把 `nginx` 和 `php-fpm` 合并在一起了, 因为测试发现 `nginx` + `php-fpm` 各开一个服务的速度
比 `oneinstack` 要慢很多, 于是就想着合并到一个镜像里面会不会好一点, 发现确实好了很多, 就有了这个项目

以下测试结果仅供参考, 为了保证可信最好还是自己再测一遍

测试命令: `docker run --rm --net host httpd:alpine ab -n 100000 -c 5000 http://192.168.1.102/` 
注:
- 发起 `ab` 测试 和 提供服务 的服务器是两台不同的服务器
- 服务器型号是阿里云的 `ecs.sn1ne.xlarge`


测试模式                            | 完成时间
----------------------------------- | ---------------------
`docker swarm`: `nginx` + `php-fpm` | 80s
`docker swarm`: `nginx+php-fpm`     | 25s
`rancher k8s`:  `nginx+php-fpm`     | 24s
`oneinstack`                        | 10s

# 可配置项

在 [`Dockerfile`](./Dockerfile) 里的 `ENV` 配置项找