# CHANGELOG

## 2.2.0

### ADD

- 添加了 `X-Frame-Options` 响应头防止点击攻击
- 隐藏了 `php` 响应头中的版本号

## 2.0.2

### FIX

- 更新 `shynome/nginx` 基础镜像以修复 nginx include 不存在的文件导致进程退出的问题

## 2.0.1

### FIX

- `php-fpm.conf` 的注释用的是分号

## 2.0.0

### BREAK CHANGE

- 现在使用 `devilbox/php-fpm` 为基础镜像了, 因为我对 `php` 不是特别熟悉, 所以使用维护的比较好的 `php-fpm` 镜像为基础镜像是比较舒心的.   
  因为 `devilbox/php-fpm` 自带了 `supervisord` 这个 `Python` 的多进程管理器, 所以把 `nodejs` 的 `pm2` 给替换掉了

## 1.1.0

### ADD

- 添加 `gd` 图片处理库 

